package j1017;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import j1017.component.LoggerComponent;

@RestController
@SpringBootApplication
public class App implements CommandLineRunner {
	@Autowired
	private Logger logger;

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@RequestMapping("/index")
	public String index() {
		logger.trace("We're in index");
		return "indexPAMPAM";
	}

	@Override
	public void run(String... args) throws Exception {
		logger.trace("Application is started");

	}
}