package j1017.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import j1017.model.Robot;

@Repository
public class RobotRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Robot getRobot() {
		return new Robot();
	}
}
